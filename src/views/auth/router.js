const authRouter = [
    {
        path: "/login",
        name: "login",
        component: () => import(/* webpackChunkName: "login" */ "./login"),
    },
    {
        path: "/register",
        name: "register",
        component: () =>
            import(/* webpackChunkName: "recuperarPassword" */ "./register"),
    },
    {
        path: "/autenticacionqr",
        name: "2fa",
        component: () => import(/* webpackChunkName: "qr" */ "./2fa"),
    },
]

export default authRouter;