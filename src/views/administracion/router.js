const adminRouter = [
    {
        path: "/perfil",
        name: "perfil",
        component: () =>
            import(/* webpackChunkName: "perfil" */ "./perfil"),
    },
    {
        path: "/seguridad",
        name: "seguridad",
        component: () =>
            import(/* webpackChunkName: "seguridad" */ "./seguridad.vue"),
    },
]


export default adminRouter;