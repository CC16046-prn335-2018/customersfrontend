export default [
    {
        path: "cases",
        name: "cases-list",
        component: () => import(/* webpackChunkName: "login" */ "./cases"),
    },
    {
        path: "cases/combined",
        name: "CombinedCases",
        component: () => import(/* webpackChunkName: "login" */ "./CombinedCases"),
    },
]
