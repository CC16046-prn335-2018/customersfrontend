import {http_client} from "./http_client";
import Vue from "vue";

const getPais = async (params = {}) => {
    return await http_client('/api/v1/pais',params,'get')
}

const getDepartamento = async (params = {}) => {
    return await http_client('/api/v1/departamento',params,'get')
}

const getMunicipio = async (params = {}) => {
    return await http_client('/api/v1/municipio',params,'get')
}

const getSexo = async (params = {}) => {
    return await http_client('/api/v1/sexo',params,'get')
}

const getProducto = async (params = {}) =>  {
    return await http_client('/api/v1/productos',params,'get')
}

const createProducto = async (params = {}) =>  {
    return await http_client('/api/v1/productos',params,'post')
}

const updateProducto = async (id_producto,params = {}) =>  {
    return await http_client(`/api/v1/productos/${id_producto}`,params,'put')
}

const deleteProducto = async (id_producto,params = {}) => {
    return await http_client(`/api/v1/productos/${id_producto}`,params,'delete')
}

const getInventariosProducto = async (params = {}) =>  {
    return await http_client('api/v1/productos/inventario_producto',params,'get')
}

const getPersonas = async (params = {}) =>  {
    return await http_client('/api/v1/personas',params,'get')
}

const getPersona = async (id_persona = '',params = {} ) =>  {
    return await http_client(`/api/v1/personas/${id_persona}`,params,'get')
}

const createPersona = async (params = {}) => {
    return await http_client('/api/v1/personas',params,'post')
}

const updatePersona = async (id_persona,params = {}) => {
    return await http_client(`/api/v1/personas/${id_persona}`,params,'put')
}

const deletePersona = async (id_persona) => {
    return await http_client(`/api/v1/personas/${id_persona}`,{},'delete')
}

const setPersonaDocumento = async (id_persona,id_documento,params = {}) => {
    return await http_client(`/api/v1/personas/${id_persona}/documentos/${id_documento}`,params,'put')
}
const setPersonaTipoDocumento = async (id_persona,id_documento,params = {}) => {
    return await http_client(`/api/v1/personas/${id_persona}/tipo_documento/${id_documento}`,params,'put')
}

const getGruposFuenteFinanc = async (params = {}) => {
    return await http_client('/api/v1/grupo_fuente_financiamiento',params,'get')
}

const getFuenteFinanciamiento = async (params = {}) => {
    return await http_client('/api/v1/fuente_financiamiento',params,'get')
}

const getProveedores = async (params = {}) => {
    return await http_client('/api/v1/proveedor',params,'get')
}

const getProveedor = async (id_proveedor,params = {}) => {
    return await http_client(`/api/v1/proveedor/${id_proveedor}`,params,'get')
}

const createProveedor = async (params = {}) =>  {
    return await http_client('/api/v1/proveedor',params,'post')
}

const updateProveedor = async (id_proveedor,params = {}) =>  {
    return await http_client(`/api/v1/proveedor/${id_proveedor}`,params,'put')
}

const deleteProveedor = async (id_delete) => {
    return await http_client(`/api/v1/proveedor/${id_delete}`,{},'delete')
}

const getCategoria = async (params = {}) => {
    return await http_client('/api/v1/categorias',params,'get')
}

const createCategoria = async (params = {}) => {
    return await http_client('/api/v1/categorias',params,'post')
}

const updateCategoria = async (id_categoria,params = {}) => {
    return await http_client(`/api/v1/categorias/${id_categoria}`,params,'put')
}

const deleteCategoria = async (id_categoria) => {
    return await http_client(`/api/v1/categorias/${id_categoria}`,{},'delete')
}

const getMarcas = async (params = {} ) =>  {
    return await http_client(`/api/v1/marcas/`,params,'get')
}

const getMarca = async (id_marca,params = {} ) =>  {
    return await http_client(`/api/v1/marcas/${id_marca}`,params,'get')
}

const createMarca = async (params = {}) =>  {
    return await http_client(`/api/v1/marcas/`,params,'post')
}

const updateMarca = async (id_proveedor,params = {}) =>  {
    return await http_client(`/api/v1/marcas/${id_proveedor}`,params,'put')
}

const deleteMarca = async (id_delete) => {
    return await http_client(`/api/v1/marcas/${id_delete}`,'','delete')
}

const getDependencia = async (id_dependencia,params = {}) => {
    return await http_client(`/api/v1/dependencias/${id_dependencia}`,params,'get')
}

const getDependencias = async (params = {}) => {
    return await http_client('/api/v1/dependencias',params,'get')
}

const getDependenciaAmbientes = async (id_dependencia,params = {}) => {
    return await http_client(`/api/v1/dependencias/${id_dependencia}/ambientes`,params,'get')
}


const createDependencia = async (params = {}) =>  {
    return await http_client('/api/v1/dependencias',params,'post')
}

const updateDependencia = async (id_dependencia,params = {}) =>  {
    return await http_client(`/api/v1/dependencias/${id_dependencia}`,params,'put')
}

const deleteDependencia = async (id_delete, params = {}) => {
    return await http_client(`/api/v1/dependencias/${id_delete}`,params,'delete')
}

const getInstitucion = async (params = {}) => {
    return await http_client('/api/v1/institucion',params,'get')
}

const getTipoDependencia = async (params = {}) => {
    return await http_client('/api/v1/tipo_dependencia',params,'get')
}



const getTipoIngreso = async (params = {}) => {
    return await http_client('/api/v1/tipo_ingreso',params,'get')
}

const getMotivoUso = async (params = {}) => {
    return await http_client('/api/v1/motivo_uso',params,'get')
}

const createMotivoUso = async (params = {}) =>  {
    return await http_client('/api/v1/motivo_uso',params,'post')
}

const updateMotivoUso = async (id_motivouso,params = {}) =>  {
    return await http_client(`/api/v1/motivo_uso/${id_motivouso}`,params,'put')
}

const deleteMotivoUso = async (id_motivouso,params = {}) => {
    return await http_client(`/api/v1/motivo_uso/${id_motivouso}`,params,'delete')
}

const getAutores = async (params = {}) => {
    return await http_client('/api/v1/autor',params,'get')
}

const createAutores = async (params = {}) =>  {
    return await http_client('/api/v1/autor',params,'post')
}

const cargarImg = async (id, params = {}) => {
    return await http_client(`/api/v1/ingresos/${id}/guardar-imagen`, params,'put')
}

const eliminarImg = async (id, params = {}) => {
    return await http_client(`api/v1/ingresos/${id}/eliminar_imagen`, params,'delete')
}

const updateAutores = async (id_autor,params = {}) =>  {
    return await http_client(`/api/v1/autor/${id_autor}`,params,'put')
}

const deleteAutores = async (id_autor,params = {}) => {
    return await http_client(`/api/v1/autor/${id_autor}`,params,'delete')
}

const getAutor = async (id_autor = '',params = {} ) =>  {
    return await http_client(`/api/v1/autor/${id_autor}`,params,'get')
}

const getAutorLicencias = async (id_autor = '',params = {}) =>  {
    return await http_client(`api/v1/autor/${id_autor}/licencias`,params,'get')
}

const getCargos = async (params = {}) =>  {
    return await http_client('/api/v1/cargos',params,'get')
}

const getCargo = async (id_cargo = '',params = {}) => {
    return await http_client(`/api/v1/cargos/${id_cargo}`,params,'get')
}

const createCargo = async (params = {}) => {
    return await http_client('/api/v1/cargos',params,'post')
}

const updateCargo = async (id_cargo,params = {}) => {
    return await http_client(`/api/v1/cargos/${id_cargo}`,params,'put')
}

const deleteCargo = async (id_cargo) => {
    return await http_client(`/api/v1/cargos/${id_cargo}`,{},'delete')
}

const getAmbiente = async (id_ambiente,params = {}) => {
    return await http_client(`/api/v1/ambientes/${id_ambiente}`,params,'get');
}

const getAmbientes = async (params = {}) => {
    return await http_client(`/api/v1/ambientes`,params,'get');
}

const createAmbiente = async (params = {}) => {
    return await http_client('/api/v1/ambientes',params,'post')
}

const updateAmbiente = async (id_ambiente,params = {}) => {
    return await http_client(`/api/v1/ambientes/${id_ambiente}`,params,'put')
}

const deleteAmbiente = async (id_ambiente) => {
    return await http_client(`/api/v1/ambientes/${id_ambiente}`,{},'delete')
}

const getDependenciasEstablecimiento = async (id_establecimiento = '',params = {} ) =>  {
    return await http_client(`/api/v1/establecimientos/${id_establecimiento}/dependencias`,params,'get')
}

const getEstablecimientos = async (params = {} ) =>  {
    return await http_client(`/api/v1/establecimientos/`,params,'get')
}

const getEstablecimiento = async (id_establecimiento = '',params = {} ) =>  {
    return await http_client(`/api/v1/establecimientos/${id_establecimiento}`,params,'get')
}

const createEstablecimiento = async (params = {}) => {
    return await http_client('/api/v1/establecimientos',params,'post')
}

const updateEstablecimiento = async (id_establecimiento,params = {}) => {
    return await http_client(`/api/v1/establecimientos/${id_establecimiento}`,params,'put')
}

const deleteEstablecimiento = async (id_establecimiento) => {
    return await http_client(`/api/v1/establecimientos/${id_establecimiento}`,{},'delete')
}

const getTipoEstablecimiento = async (params = {}) =>  {
    return await http_client('/api/v1/tipo_establecimiento',params,'get')
}

const getIngresos = async (params = {}) => {
    return await http_client(`/api/v1/ingresos`, params, 'get')
}

const getIngreso = async (id) => {
    return await http_client(`/api/v1/ingresos/${id}`,{},'get')
}

const createIngreso = async (params = {}) => {
    return await http_client('/api/v1/ingresos',params,'post')
}

const updateIngreso = async (id_ingreso,params = {}) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}`,params,'put')
}

const deleteIngreso = async (id_ingreso) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}`,{},'delete')
}

const getIngresosLotes = async (id_ingreso,params = {}) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}/lotes`,params,'get')
}

const getIngresoLote = async (id_ingreso,id_lote) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}`,{},'get')
}

const createIngresosLotes = async (id_ingreso,params = {}) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}/lotes`,params,'post')
}

const updateIngresosLotes = async (id_ingreso,id_lote,params = {}) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}`, params, 'put')
}

const deleteIngresosLotes = async (id_ingreso,id_lote,params = {}) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}`, params, 'delete')
}

const getIngresoLoteDetalle = async (id_ingreso, id_lote, params) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles`,params,'get')
}

const getIngresoLoteDetalleAgrupado = async (id_ingreso, id_lote, params) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles_agrupados`,params,'get')
}

const createIngresoLoteDetalle = async (id_ingreso,id_lote,params = {}) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles`,params,'post')
}


const createIngresoLoteDetalleAgrupado = async (id_ingreso,id_lote,params = {}) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles_agrupados`,params,'post')
}

const createIngresoLoteDetallesAgrupados = async (id_ingreso,id_lote,params = {}) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles_agrupados`,params,'post')
}

const createIngresoLoteDetallesSimples = async (id_lote,params = {}) => {
    return await http_client(`/api/v1/ingresos/lotes/${id_lote}/detalles`,params,'post')
}

const updateIngresoLoteDetallesAgrupados = async (id_lote,params = {}) => {
    return await http_client(`/api/v1/ingresos/lotes/{id_lote}/detalles/{id_detalle_ingreso}/agrupados`,params,'post')
}


const updateIngresoLoteDetalle = async (id_ingreso,id_lote,id_detalle,params = {}) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles/${id_detalle}`,params,'put')
}

const deleteIngresoLoteDetalle = async (id_ingreso,id_lote,id_detalle,params = {}) => {
    return await http_client(`api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles/${id_detalle}`,params,'delete')
}

const setEstadoIngresos = async (id_ingreso,params = {}) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}/estado/`,params,'put')
}

const getEstadoIngresos = async (params = {}) => {
    return await http_client('/api/v1/ingresos/estados',params,'get')
}

const leerImg = async (params = {}) => {
    return await http_client(`/api/v1/cargar-imagen`,params,'get')
}

const loadIngresosCSV = async (params = {}) => {
    return await http_client('/api/v1/ingresos/cargar_csv', params, 'post')
}

const cargarVale = async ( numero_vale,params={}) => {

    return await http_client(`/api/v1/ingresos/cargar_vale/${numero_vale}`, params,'post')
}

const getLicencias = async (params = {}) => {
    return await http_client('/api/v1/licencia', params, 'get')
}

const getLicencia = async (id_licencia='', params={}) => {
    return await http_client(`/api/v1/licencia/${id_licencia}`, params, 'get')
}

const createLicencia = async (params = {}) => {
    return await http_client('/api/v1/licencia',params,'post')
}

const updateLicencia = async (id_licencia,params = {}) => {
    return await http_client(`/api/v1/licencia/${id_licencia}`,params,'put')
}

const deleteLicencia = async (id_licencia) => {
    return await http_client(`/api/v1/licencia/${id_licencia}`,{},'delete')
}

const getTiposLicencias = async (params = {}) => {
    return await http_client('/api/v1/tipo_licencia', params, 'get')
}

const getTipoLicencia = async (id_tipo_licencia = '', params = {}) => {
    return await http_client(`api/v1/tipo_licencia/${id_tipo_licencia}`, params, 'get')
}

const createTiposLicencia = async (params = {}) => {
    return await http_client('/api/v1/tipo_licencia/',params,'post')
}

const updateTiposLicencia = async (id_tipo_licencia,params = {}) => {
    return await http_client(`/api/v1/tipo_licencia/${id_tipo_licencia}`,params,'put')
}

const deleteTiposLicencia = async (id_tipo_licencia) => {
    return await http_client(`/api/v1/tipo_licencia/${id_tipo_licencia}`,{},'delete')
}

const getTiposLicencia = async (params = {},id_licencia) => {
    return await http_client(`/api/v1/licencia/${id_licencia}/tipos`, params, 'get')
}

const getEstadoIngresoDetalle = async (id_ingreso,id_lote,params = {}) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles/estado`,params,'get')
}

const getEstadosLoteDetalles = async () => {
    return await http_client(`/ingresos/lotes/detalles/estado`,{},'get')
}

const getDetalleIngreso = async (serial,params = {}) => {
    return await http_client(`/api/v1/ingresos/inventario_producto/${serial}`,params, 'get')
}

const EditarIngreso = async (params = {}, id) => {
    return await http_client(`/api/v1/ingresos/${id}`,params,'put')
}

const RegistrarLoteDetalle = async (id_ingreso, id_lote, params = {}) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles`,params,'post')
}


const RegistrarLoteDetalleAgrupados = async (id_ingreso, id_lote, params = {}) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles_agrupados`,params,'post')
}


const RegistrarIngresoLote = async (params = {}, id) => {
    return await http_client(`/api/v1/ingresos/${id}/lotes`,params,'post')
}

const EditarLoteDetalle = async (id_ingreso, id_lote, id_detalle, params = {}) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}/lotes/${id_lote}/detalles/${id_detalle}`,params,'put')
}

const EditarLoteDetalleAgrupado = async (id_lote,id_detalle, params = {}) => {
    return await http_client(`/api/v1/ingresos/lotes/${id_lote}/detalles/${id_detalle}/agrupados`,params,'put')
}

const EliminarIngresoLote = async (id_ingreso, id_lote) => {
    return await http_client(`/api/v1/ingresos/${id_ingreso}/lotes/${id_lote}`,{},'delete')
}

const getEgresos = async (params = {} ) =>  {
    return await http_client(`/api/v1/egreso/`,params,'get')
}

const getEgreso = async (id_egreso,params = {} ) =>  {
    return await http_client(`/api/v1/egreso/${id_egreso}`,params,'get')
}

const createEgreso  = async (params = {}) => {
    return await http_client(`/api/v1/egreso`,params,'post')
}

const updateEgreso = async (id_egreso,params = {}) => {
    return await http_client(`/api/v1/egreso/${id_egreso}`,params,'put')
}

const deleteEgreso = async (id_egreso) => {
    return await http_client(`/api/v1/egreso/${id_egreso}`,{},'delete')
}

const updateEstadoEgreso = async (id_egreso,params = {}) => {
    return await http_client(`/api/v1/egreso/${id_egreso}/estado`,params,'put')
}

const getEgresoDetalles = async (id_egreso,params = {}) =>  {
    return await http_client(`/api/v1/egreso/${id_egreso}/detalles`,params,'get')
}

const getEgresoDetalle = async (id_egreso,id_detalle) =>  {
    return await http_client(`/api/v1/egreso/${id_egreso}/detalles/${id_detalle}`,{},'get')
}

const createEgresoDetalle = async (id_egreso,params = {}) =>  {
    return await http_client(`/api/v1/egreso/${id_egreso}/detalles`,params,'post')
}

const updateEgresoDetalle = async (id_egreso,id_detalle,params = {}) =>  {
    return await http_client(`/api/v1/egreso/${id_egreso}/detalles/${id_detalle}`,params,'put')
}

const deleteEgresoDetalle = async (id_egreso,id_detalle) =>  {
    return await http_client(`/api/v1/egreso/${id_egreso}/detalles/${id_detalle}`,{},'delete')
}

const sobreescribirEgreso = async (id_egreso) => {
    return await http_client(`/api/v1/egreso/${id_egreso}/sobreescribir`,{},'post')
}

const setSerialEgreso = async (id_egreso,id_detalle,params = {}) => {
    return await http_client(`api/v1/egreso/${id_egreso}/detalles/${id_detalle}/set_Serial`,params,'put')
}

const getTipoEgreso = async (params = {}) => {
    return await http_client('/api/v1/egreso/tipos',params,'get')
}

const setEstadoEgreso = async (id_egreso,params = {}) =>  {
    return await http_client(`/api/v1/egreso/${id_egreso}/set-estado`,params,'put')
}

const updateEgresoNuevaEntrega = async (id_egreso,params = {}) =>  {
    return await http_client(`/api/v1/egreso/${id_egreso}/nueva_entrega`,params,'put')
}

const getArea = async (id_area = '',params = {} ) =>  {
    return await http_client(`/api/v1/areas/${id_area}`,params,'get')
}

const getAreas = async (params = {} ) =>  {
    return await http_client(`/api/v1/areas/`,params,'get')
}

const createArea = async (params = {}) => {
    return await http_client('/api/v1/areas',params,'post')
}

const updateArea = async (id_area,params = {}) => {
    return await http_client(`/api/v1/areas/${id_area}`,params,'put')
}

const deleteArea = async (id_area) => {
    return await http_client(`/api/v1/areas/${id_area}`,{},'delete')
}

const getPerfiles = async (params = {}) =>  {
    return await http_client('/api/v1/perfiles',params,'get')
}

const getRoles = async (id_rol,params = {}) =>  {
    return await http_client(`/api/v1/roles${id_rol}`,params,'get')
}

const getDevoluciones = async (params = {} ) =>  {
    return await http_client(`/api/v1/devoluciones/`,params,'get')
}

const getDevolucion = async (id_devolucion) =>  {
    return await http_client(`/api/v1/devoluciones/${id_devolucion}`,{},'get')
}

const createDevolucion = async (params = {}) => {
    return await http_client('/api/v1/devoluciones',params,'post')
}

const updateDevolucion = async (id_devolucion,params = {}) => {
    return await http_client(`/api/v1/devoluciones/${id_devolucion}`,params,'put')
}

const deleteDevolucion = async (id_devolucion) => {
    return await http_client(`/api/v1/devoluciones/${id_devolucion}`,{},'delete')
}

const getDevolucionDetalles = async (id_devolucion) =>  {
    return await http_client(`/api/v1/devoluciones/detalles/${id_devolucion}`,{},'get')
}

const getDevolucionDetalle = async (id_devolucion,id_detalle) =>  {
    return await http_client(`/api/v1/devoluciones/detalles/${id_devolucion}/${id_detalle}`,{},'get')
}

const createDevolucionDetalle = async (id_devolucion,params = {}) => {
    return await http_client(`/api/v1/devoluciones/detalles/${id_devolucion}/`,params,'post')
}

const updateDevolucionDetalle = async (id_devolucion,id_detalle,params = {}) => {
    return await http_client(`/api/v1/devoluciones/detalles/${id_devolucion}/${id_detalle}`,params,'put')
}

const deleteDevolucionDetalle = async (id_devolucion,id_detalle) => {
    return await http_client(`/api/v1/devoluciones/detalles/${id_devolucion}/${id_detalle}`,{},'delete')
}


const getDevolucionesMotivos = async (params = {} ) =>  {
    return await http_client(`/api/v1/devoluciones/motivos`,params,'get')
}

const createDevolucionesMotivo = async (params = {}) => {
    return await http_client('/api/v1/devoluciones/motivos',params,'post')
}

const updateDevolucionesMotivo = async (id_motivo,params = {}) => {
    return await http_client(`/api/v1/devoluciones/motivos/${id_motivo}`,params,'put')
}

const getUsers = async (params = {} ) =>  {
    return await http_client(`/api/v1/users`,params,'get')
}

const getUser = async (id_usuario,params = {} ) =>  {
    return await http_client(`/api/v1/users/${id_usuario}`,params,'get')
}

const getUserNotificaciones = async (id_usuario,params = {} ) =>  {
    return await http_client(`/api/v1/users/${id_usuario}/notificaciones`,params,'get')
}

const deleteUserNotificacion = async (id_usuario,params = {} ) =>  {
    return await http_client(`/api/v1/notificaciones/${id_usuario}/usuarios`,params,'delete')
}

const getNotificaciones = async (params = {}) =>  {
    return await http_client(`/api/v1/notificaciones`,params,'get')
}

const getNotificacion = async (id_notificacion) => {
    return await http_client(`/api/v1/notificaciones/${id_notificacion}`,{},'get')
}

const createNotificacion = async (params = {}) => {
    return await http_client('/api/v1/notificaciones',params,'post')
}

const updateNotificacion = async (id_notificacion,params = {}) => {
    return await http_client(`/api/v1/notificaciones/${id_notificacion}`,params,'put')
}

const getTipoNotificacion = async (id_tipo) => {
    return await http_client(`/api/v1/notificaciones/tipos/${id_tipo}`,{},'get')
}

const getTiposNotificacion = async () => {
    return await http_client(`/api/v1/notificaciones/tipos`,{},'get')
}

const createUsuarioNotificacion = async (id_notificacion,params = {}) => {
    return await http_client(`/api/v1/notificaciones/${id_notificacion}/usuarios`,params,'post')
}

const deleteUsuarioNotificacion = async (id_notificacion,params = {}) => {
    return await http_client(`/api/v1/notificaciones/${id_notificacion}/usuarios`,params,'delete')
}

const setEstadoNotificacionSeguimiento = async (id_notificacion,id_usuario) => {
    return await http_client(`/api/v1/notificaciones/${id_notificacion}/users/${id_usuario}/set_estado_seguimiento`,{},'put')
}

const createEgresoCodigoVerificacion = async (params = {}) => {
    return await http_client(`/api/v1/egreso/codigo_verificacion`,params,'post')
}

const createEgresoAsignarResponsable = async (id_egreso, params = {}) => {
    return await http_client(`/api/v1/egreso/${id_egreso}/asignar-responsable`,params,'post')
}

const getProductosAgrupados = async (id_padre, params = {}) =>  {
    return await http_client(`/api/v1/productos/${id_padre}/productos-agrupado`,params,'get')
}

const getTipoProducto = async (params = {}) => {
    return await http_client('/api/v1/tipo_producto',params,'get')
}

const getProductoItem = async (id_producto="", params={}) => {
    return await http_client(`/api/v1/productos/${id_producto}`,params,'get')
}


const createProductoAgrupado = async (id_padre, params = {}) =>  {
    return await http_client(`/api/v1/productos/${id_padre}/productos-agrupado`,params,'post')
}

const updateProductosAgrupados = async (id_padre,params = {}) =>  {
    return await http_client(`/api/v1/productos/${id_padre}/productos-agrupado`,params,'put')
}


Vue.prototype.services = {
    createEgresoAsignarResponsable,
    createEgresoCodigoVerificacion,
    EliminarIngresoLote,
    EditarIngreso,
    EditarLoteDetalle,
    EditarLoteDetalleAgrupado,
    getIngresoLoteDetalle,
    getIngresoLoteDetalleAgrupado,
    getIngresoLote,
    getLicencias,
    getLicencia,
    createLicencia,
    updateLicencia,
    deleteLicencia,
    getTiposLicencia,
    getTiposLicencias,
    getTipoLicencia,
    createTiposLicencia,
    updateTiposLicencia,
    deleteTiposLicencia,
    loadIngresosCSV,
    leerImg,
    getIngreso,
    getIngresos,
    getEstadoIngresos,
    setEstadoIngresos,
    getPais,
    getDepartamento,
    getMunicipio,
    getSexo,
    getProducto,
    createProducto,
    updateProducto,
    deleteProducto,
    getInventariosProducto,
    getPersonas,
    getPersona,
    createPersona,
    updatePersona,
    deletePersona,
    setPersonaDocumento,
    setPersonaTipoDocumento,
    getGruposFuenteFinanc,
    getFuenteFinanciamiento,
    getProveedor,
    getProveedores,
    createProveedor,
    updateProveedor,
    deleteProveedor,
    getAutores,
    cargarImg,
    eliminarImg,
    createAutores,
    updateAutores,
    deleteAutores,
    getAutor,
    getAutorLicencias,
    getCategoria,
    createCategoria,
    updateCategoria,
    deleteCategoria,
    getMarca,
    createMarca,
    updateMarca,
    deleteMarca,
    getDependencia,
    getDependencias,
    getDependenciaAmbientes,
    createDependencia,
    updateDependencia,
    deleteDependencia,
    getTipoDependencia,
    getInstitucion,
    getTipoEgreso,
    getMotivoUso,
    createMotivoUso,
    updateMotivoUso,
    deleteMotivoUso,
    getCargos,
    getCargo,
    createCargo,
    updateCargo,
    deleteCargo,
    getAmbiente,
    getAmbientes,
    createAmbiente,
    updateAmbiente,
    deleteAmbiente,
    getEstablecimientos,
    getEstablecimiento,
    createEstablecimiento,
    updateEstablecimiento,
    deleteEstablecimiento,
    getTipoEstablecimiento,
    createIngreso,
    updateIngreso,
    deleteIngreso,
    getIngresosLotes,
    createIngresosLotes,
    updateIngresosLotes,
    deleteIngresosLotes,
    createIngresoLoteDetalle,
    createIngresoLoteDetalleAgrupado,
    createIngresoLoteDetallesAgrupados,
    updateIngresoLoteDetallesAgrupados,
    createIngresoLoteDetallesSimples,
    updateIngresoLoteDetalle,
    deleteIngresoLoteDetalle,
    getEstadoIngresoDetalle,
    getDetalleIngreso,
    getEstadosLoteDetalles,
    getDependenciasEstablecimiento,
    getEgresos,
    getEgreso,
    createEgreso,
    updateEgreso,
    deleteEgreso,
    getEgresoDetalle,
    getEgresoDetalles,
    createEgresoDetalle,
    updateEgresoDetalle,
    deleteEgresoDetalle,
    setEstadoEgreso,
    updateEgresoNuevaEntrega,
    sobreescribirEgreso,
    updateEstadoEgreso,
    RegistrarLoteDetalle,
    RegistrarLoteDetalleAgrupados,
    RegistrarIngresoLote,
    getTipoIngreso,
    getArea,
    getAreas,
    createArea,
    updateArea,
    deleteArea,
    getPerfiles,
    getRoles,
    getDevoluciones,
    getDevolucion,
    createDevolucion,
    updateDevolucion,
    deleteDevolucion,
    getDevolucionDetalles,
    getDevolucionDetalle,
    createDevolucionDetalle,
    updateDevolucionDetalle,
    deleteDevolucionDetalle,
    getDevolucionesMotivos,
    createDevolucionesMotivo,
    updateDevolucionesMotivo,
    getUser,
    getUsers,
    getNotificacion,
    getNotificaciones,
    createNotificacion,
    updateNotificacion,
    cargarVale,
    getTiposNotificacion,
    getTipoNotificacion,
    createUsuarioNotificacion,
    deleteUsuarioNotificacion,
    getUserNotificaciones,
    deleteUserNotificacion,
    setEstadoNotificacionSeguimiento,
    setSerialEgreso,
    getMarcas,
    createProductoAgrupado,
    updateProductosAgrupados,
    getProductosAgrupados,
    getTipoProducto,
    getProductoItem
}
